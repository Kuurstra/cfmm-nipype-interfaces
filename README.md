# Welcome to CFMM's custom interfaces for nipype!

This is a collection of custom interfaces made by students for their nipype pipelines.  
Nipype pipelines in conjuction with containers (docker/singularity) encourage the reproducibility of your work.

Here are some of the other reasons students use this approach: 
* Easy distrbution and adoption of your work (and more citations)
* Quicker development and debugging of your pipeline  - nipype caches steps and only recomputes what it needs to
* Built-in error checking - nipype ensures each step's indicated outputs are actually generated
* Reproducibility - Wrapping pipelines in containers ensures reproducibility since the operating system and all software versions are the same for everyone who uses your pipeline.
* Quick processing of multiple subjects - wrapping your pipeline in a container allows it to be used on Compute Canada which allows for parallel processing of multiple subjects
* Documentation - so future students can build on it and it continues to be used after you leave the institution

Check out the **cfmm-nipype-interfaces/cfmm_nipype_interfaces/example** directory for some simple examples of custom nipype interfaces.
