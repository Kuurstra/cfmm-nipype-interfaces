import numpy as np


def rotate_ellipsoid(coords, phi=0, axes=0):
    # Rodrigues' rotation formula
    # look down axis at origin, rotation happens cw

    if phi == 0 or axes == 0:
        return coords

    X, Y, Z = coords
    axes = axes / np.linalg.norm(axes)
    Xrot = X * np.cos(phi) + (axes[1] * Z - axes[2] * Y) * np.sin(phi) + axes[0] * (
            X * axes[0] + Y * axes[1] + Z * axes[2]) * (1 - np.cos(phi))
    Yrot = Y * np.cos(phi) + (axes[2] * X - axes[0] * Z) * np.sin(phi) + axes[1] * (
            X * axes[0] + Y * axes[1] + Z * axes[2]) * (1 - np.cos(phi))
    Zrot = Z * np.cos(phi) + (axes[0] * Y - axes[1] * X) * np.sin(phi) + axes[2] * (
            X * axes[0] + Y * axes[1] + Z * axes[2]) * (1 - np.cos(phi))
    return (Xrot, Yrot, Zrot)


def create_ellipsoid(FOV, rad, dim_sz, center=(0, 0, 0), angle=0, axes=0):
    radx, rady, radz = rad
    x = np.linspace(-FOV / 2, FOV / 2, dim_sz)
    y = np.linspace(-FOV / 2, FOV / 2, dim_sz)
    z = np.linspace(-FOV / 2, FOV / 2, dim_sz)
    Y, X, Z = np.meshgrid(y, x, z)
    X -= center[0]
    Y -= center[1]
    Z -= center[2]
    X, Y, Z = rotate_ellipsoid((X, Y, Z), angle, axes)
    ellipsoid = ((X / radx) ** 2 + (Y / rady) ** 2 + (Z / radz) ** 2) < 1
    return ellipsoid.astype('float')
