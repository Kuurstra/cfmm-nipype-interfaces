from enum import Enum

class MatlabRunMode(Enum):
    LOCAL_MATLAB = 1
    LOCAL_MCR = 2
    CONTAINER_MCR = 3
