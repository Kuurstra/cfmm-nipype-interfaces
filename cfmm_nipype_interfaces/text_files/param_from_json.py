"""
gets any parameter from json files (for BIDS)
"""

from nipype.interfaces.utility import Function


def get_param_from_json_fn(filename, parameter):
    import json
    if type(filename) == list:
        filename = filename[0]
    with open(filename) as f:
        param_value = float(json.load(f)[parameter])
    f.close()
    return param_value


GetParamFromJson = Function(input_names=["filename", "parameter"],
                            output_names=["parameter_value"],
                            function=get_param_from_json_fn)


def get_CF_from_json_fn(filename):
    from cfmm_nipype_interfaces.text_files.param_from_json import get_param_from_json_fn
    return get_param_from_json_fn(filename, "ImagingFrequency") * 1e6


GetCFFromJson = Function(input_names=["filename"],
                         output_names=["CF_value"],
                         function=get_CF_from_json_fn
                         )
