"""
use pyqsm package to estimate the frequency from phase images
"""

import traits.api as traits
import nibabel as nib
import numpy as np
import os, sys
import pyqsm.frequencyEstimate
from nipype.interfaces.base import CommandLineInputSpec, BaseInterface, TraitedSpec, InputMultiPath, File
from nipype.interfaces.base.traits_extension import isdefined
import os.path
import json


class EstimateFrequencyFromWrappedPhaseInputSpec(CommandLineInputSpec):
    phase = InputMultiPath(File(exists=True), desc='A list of phase echoes',
                           argstr="--phase %s", position=1,
                           copyfile=False, mandatory=True)
    json = InputMultiPath(File(exists=True), desc='A list of json files containing the tes',
                          argstr="--phase %s", position=1,
                          copyfile=False, mandatory=True)
    truncate_echo = traits.Int(0, desc='Cut off echoes after this point', argstr='-truncate %s',
                               position=3, mandatory=False, usedefault=True)
    mask = File(desc='Mask (3D) where to estimate frequency', argstr="--mask %s",
                position=4, mandatory=True)
    weight = File(desc='Weights (4D) for phase measurements', argstr="--weight %s",
                  position=5, mandatory=False)

    freq_filename = File(desc="Output file", argstr='--freq %s', position=6,
                         mandatory=True)


class EstimateFrequencyFromWrappedPhaseOutputSpec(TraitedSpec):
    freq_filename = File(desc="Output file containing the estimated Frequency",
                         exists=True)


class EstimateFrequencyFromWrappedPhase(BaseInterface):
    input_spec = EstimateFrequencyFromWrappedPhaseInputSpec
    output_spec = EstimateFrequencyFromWrappedPhaseOutputSpec

    def _run_interface(self, runtime):
        phase_filename_list = self.inputs.phase
        json_filename_list = self.inputs.json
        truncate_echo = self.inputs.truncate_echo
        if not truncate_echo:
            truncate_echo = None
        mask_filename = self.inputs.mask
        freq_filename = self._list_outputs()['freq_filename']

        if isdefined(self.inputs.weight):
            weight_filename = self.inputs.weight
            weight = nib.load(weight_filename).get_data()
        else:
            weight = None
        ne = len(phase_filename_list)
        ph_img_obj = nib.load(phase_filename_list[0])
        shape = ph_img_obj.shape
        voxel_size = ph_img_obj.header['pixdim'][1:4]
        ph_img = np.empty(shape + (ne,))
        if weight is None:
            weight = np.ones(shape + (ne,))
        count = 0
        phase_filename_list.sort()
        json_filename_list.sort()
        te = np.empty(ne)
        for imgloc, jsonloc in zip(phase_filename_list, json_filename_list):
            # assert os.path.splitext(jsonloc)[0] in os.path.splitext(imgloc)[0]
            ph_img[..., count] = nib.load(imgloc).get_data()
            with open(jsonloc) as f:
                te[count] = float(json.load(f)['EchoTime'])
            count += 1
        eps = np.sqrt(sys.float_info.min)
        ph_img = ph_img + eps
        mask = nib.load(mask_filename).get_data()
        freq = pyqsm.frequencyEstimate.estimateFrequencyFromWrappedPhase(ph_img, voxel_size, te, mask, weight,
                                                                         truncateEcho=truncate_echo)
        niftifile = nib.Nifti1Pair(freq, ph_img_obj.affine)
        nib.save(niftifile, freq_filename)
        return runtime

    def _list_outputs(self):
        outputs = self.output_spec().get()
        outputs['freq_filename'] = os.path.abspath(self.inputs.freq_filename)
        return outputs
