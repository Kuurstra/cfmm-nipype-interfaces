"""
SiemensPhasePreprocess interface is for converting siemens dicom to radians and any making sure the phase is in right handed system
"""

import nibabel as nib
import numpy as np
import os
from nipype.interfaces.base import CommandLineInputSpec, BaseInterface, TraitedSpec, InputMultiPath, File, \
    OutputMultiPath
import os.path
from nipype.interfaces.base.traits_extension import isdefined


def siemens2rad(phase_img_siemens):
    # This function converts an n dimensional array from Siemens phase units to
    # radians.  Siemens employs the following phase convention:
    # S = 2048[(R/pi) + 1], where 'S' is the phase in Siemens units (i.e. what
    # is returned by the scanner) and 'R' is the phase in radians. The range of
    # any phase image, in radians, will be -pi to +pi.
    # The output, 'phase_img_rad', is of class double.

    phase_img_siemens = phase_img_siemens.astype('float');
    phase_img_rad = (phase_img_siemens / 2048 - 1) * np.pi;
    return phase_img_rad


class SiemensPhasePreprocessInputSpec(CommandLineInputSpec):
    infiles = InputMultiPath(File(exists=True), desc='A list of phase echoes',
                             copyFile=False, mandatory=True)
    output_folder = File(desc="Optional output folder for processed phase images", mandatory=False)


class SiemensPhasePreprocessOutputSpec(TraitedSpec):
    outfiles = OutputMultiPath(File(exists=True), desc='A list of vendor-specific processed phase echoes',
                               copyFile=False, mandatory=True)


class SiemensPhasePreprocess(BaseInterface):
    input_spec = SiemensPhasePreprocessInputSpec
    output_spec = SiemensPhasePreprocessOutputSpec

    def _run_interface(self, runtime):
        infiles = self.inputs.infiles
        self.outfilenames = []

        for f in infiles:
            imgobj = nib.load(f)
            img = imgobj.get_data()
            img = siemens2rad(img)
            filename, ext = os.path.splitext(f)
            ext2 = ''
            if ext == '.gz':
                filename, ext2 = os.path.splitext(filename)
            if not isdefined(self.inputs.output_folder):
                newfilename = os.path.abspath(os.path.basename(filename) + '_processed' + ext2 + ext)
            else:
                newfilename = os.path.abspath(
                    os.path.join(self.inputs.output_folder, os.path.basename(filename) + '_processed' + ext2 + ext))
            niftifile = nib.Nifti1Pair(img, imgobj.affine)
            nib.save(niftifile, newfilename)
            self.outfilenames.append(newfilename)
            print(newfilename)
        return runtime

    def _list_outputs(self):
        outputs = self.output_spec().get()
        outputs['outfiles'] = self.outfilenames
        return outputs


if 0:
    # quick and dirty but no control over input/output spec
    def SiemensPhasePreprocess_fn(infiles):
        import nibabel as nib
        import numpy as np
        import os
        def siemens2rad(phase_img_siemens):
            phase_img_siemens = phase_img_siemens.astype('float');
            phase_img_rad = (phase_img_siemens / 2048 - 1) * np.pi;
            return phase_img_rad

        outfilenames = []
        for f in infiles:
            imgobj = nib.load(f)
            img = imgobj.get_data()
            img = siemens2rad(img)
            filename, ext = os.path.splitext(f)
            newfilename = os.path.abspath(os.path.basename(filename) + '_processed' + ext)
            niftifile = nib.Nifti1Pair(img, imgobj.affine)
            nib.save(niftifile, newfilename)
            outfilenames.append(newfilename)
        return outfilenames


    SiemensPhasePreprocess = Function(input_names=["infiles"],
                                      output_names=["outfiles"],
                                      function=SiemensPhasePreprocess_fn)
