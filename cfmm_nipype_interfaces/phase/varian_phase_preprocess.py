"""
VarianPhasePreprocess makes sure the phase is in the correct handed system
"""

import nibabel as nib
import os
from nipype.interfaces.base import CommandLineInputSpec, BaseInterface, TraitedSpec, InputMultiPath, File, \
    OutputMultiPath
import os.path


class VarianPhasePreprocessInputSpec(CommandLineInputSpec):
    infiles = InputMultiPath(File(exists=True), desc='A list of phase echoes',
                             copyFile=False, mandatory=True)


class VarianPhasePreprocessOutputSpec(TraitedSpec):
    outfiles = OutputMultiPath(File(exists=True), desc='A list of vendor-specific processed phase echoes',
                               copyFile=False, mandatory=True)


class VarianPhasePreprocess(BaseInterface):
    input_spec = VarianPhasePreprocessInputSpec
    output_spec = VarianPhasePreprocessOutputSpec

    def _run_interface(self, runtime):
        infiles = self.inputs.infiles
        self.outfilenames = []

        for f in infiles:
            imgobj = nib.load(f)
            img = imgobj.get_data()
            img = -img
            filename, ext = os.path.splitext(f)
            newfilename = os.path.abspath(os.path.basename(filename) + '_processed' + ext)
            niftifile = nib.Nifti1Pair(img, imgobj.affine)
            nib.save(niftifile, newfilename)
            self.outfilenames.append(newfilename)
        return runtime

    def _list_outputs(self):
        outputs = self.output_spec().get()
        outputs['outfiles'] = self.outfilenames
        return outputs
