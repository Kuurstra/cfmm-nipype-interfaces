"""
Use second difference quality map to mask out the fieldmap voxels which are noisey.
"""

import traits.api as traits
import nibabel as nib
import numpy as np
import os
from nipype.interfaces.base import CommandLineInputSpec, BaseInterface, TraitedSpec, InputMultiPath, File
from pyunwrap3d import calculate_reliability
import os.path


class CalculateReliabilityMaskInputSpec(CommandLineInputSpec):
    phase = InputMultiPath(File(exists=True), desc='A list of phase echoes',
                           copyfile=False, mandatory=True)
    mask = File(desc='Mask (3D) where to calculate phase reliability',
                mandatory=True)
    threshold = traits.Float(3, desc='Threshold for second difference quality map',
                             mandatory=False, usedefault=True)
    reliability_mask_filename = File(desc="Output file", mandatory=True)
    reliability_filename = File(desc="Reliability output file", mandatory=True)


class CalculateReliabilityMaskOutputSpec(TraitedSpec):
    reliability_mask_filename = File(desc="Phase reliability mask", exists=True)
    reliability_filename = File(desc="The reliability map used to make the mask", exists=True)


class CalculateReliabilityMask(BaseInterface):
    input_spec = CalculateReliabilityMaskInputSpec
    output_spec = CalculateReliabilityMaskOutputSpec

    def _run_interface(self, runtime):
        phase_filename_list = self.inputs.phase
        mask_filename = self.inputs.mask
        threshold = self.inputs.threshold
        reliability_mask_filename = self._list_outputs()['reliability_mask_filename']
        reliability_filename = self._list_outputs()['reliability_filename']
        ne = len(phase_filename_list)
        ph_img_obj = nib.load(phase_filename_list[0])
        shape = ph_img_obj.shape
        ph_imgs = np.empty(shape + (ne,))
        for echo in range(ph_imgs.shape[-1]):
            ph_imgs[..., echo] = (nib.load(phase_filename_list[echo]).get_data())
        reliability = np.empty_like(ph_imgs)
        mask = (nib.load(mask_filename).get_data()).astype('bool')
        for echo in range(ph_imgs.shape[-1]):
            reliability[..., echo] = calculate_reliability(ph_imgs[..., echo].astype('float32'), mask)

        mask = mask[..., np.newaxis]
        mask.repeat(ne, axis=-1)

        if 0:
            # absolute value of second difference dependent on image mean
            # dependent on background field
            # could try using std units to avoid this
            threshold_std = self.inputs.threshold_std_units
            tmpmean = reliability[mask].mean()
            tmpmask = (reliability < tmpmean) * (reliability > 0)
            mean = reliability[tmpmask].mean()
            std = reliability[tmpmask].std()
            threshold = mean + threshold_std * std

        reliability_mask = (reliability < threshold) * mask
        reliability_mask = np.squeeze(reliability_mask)
        reliability_mask = reliability_mask.astype('int8')

        if 0:
            # smooth reliability mask to avoid noisey on/off pixel patterns
            rtmp = reliability_mask.copy()
            kernelSz_mm = 0.5  # mm
            KernelSz_vxl = np.ceil(kernelSz_mm / np.array(voxelSize)).astype('int')
            for i in range(5):
                rtmp = ndimage.gaussian_filter(rtmp.astype('float'), np.concatenate((KernelSz_vxl, (0,))))
                rtmp = rtmp * reliabilityMask
            rtmp = ndimage.gaussian_filter(rtmp.astype('float'), (0, 0, 0, .5))
            # rtmp=rtmp*reliabilityMask
            for echo in range(reliabilityMask.shape[-1]):
                rtmp[..., echo] = ndimage.binary_erosion(rtmp[..., echo])
            reliability_mask = rtmp

        niftifile = nib.Nifti1Pair(reliability_mask, ph_img_obj.affine)
        nib.save(niftifile, reliability_mask_filename)
        niftifile = nib.Nifti1Pair(reliability, ph_img_obj.affine)
        nib.save(niftifile, reliability_filename)
        return runtime

    def _list_outputs(self):
        outputs = self.output_spec().get()
        outputs['reliability_mask_filename'] = os.path.abspath(self.inputs.reliability_mask_filename)
        outputs['reliability_filename'] = os.path.abspath(self.inputs.reliability_filename)
        return outputs
