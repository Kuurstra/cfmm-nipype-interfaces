"""
The reliability calculation is constrained by eroding the original mask.
To erode the mask it is inverted, convolved with a spherical
kernel of user defined size, and then thresholded.  Reliability is calculated
in the contsrained section between original mask and eroded mask - any voxels
that are unreliable will be removed in order to create a new mask.
"""
from scipy import ndimage
import traits.api as traits
import nibabel as nib
import numpy as np
import os
from nipype.interfaces.base import CommandLineInputSpec, BaseInterface, TraitedSpec, File
from pyunwrap3d import calculate_reliability
import os.path


class TrimMaskUsingReliabilityInputSpec(CommandLineInputSpec):
    phase = File(exists=True, desc='Phase image used to calculate reliability',
                 copyfile=False, mandatory=True)
    mask = File(desc='Mask to be eroded', mandatory=True)
    erosion_sz = traits.Float(5, desc='Erosion in mm', mandatory=False, usedefault=True)
    threshold = traits.Float(0, desc='Threshold for second difference quality map',
                             mandatory=False, usedefault=True)
    trimmed_mask_filename = File(desc="Mask output file", mandatory=True)
    reliability_filename = File(desc="Reliability output file", mandatory=True)


class TrimMaskUsingReliabilityOutputSpec(TraitedSpec):
    trimmed_mask_filename = File(desc="The trimmed mask", exists=True)
    reliability_filename = File(desc="The reliability map used to make the mask", exists=True)


class TrimMaskUsingReliability(BaseInterface):
    input_spec = TrimMaskUsingReliabilityInputSpec
    output_spec = TrimMaskUsingReliabilityOutputSpec

    def _run_interface(self, runtime):
        phaseFilename = self.inputs.phase
        maskFilename = self.inputs.mask
        threshold = self.inputs.threshold
        rad = self.inputs.erosion_sz
        trimmed_mask_filename = self._list_outputs()['trimmed_mask_filename']
        reliability_filename = self._list_outputs()['reliability_filename']

        ph_img_obj = nib.load(phaseFilename)
        voxel_size = ph_img_obj.header['pixdim'][1:4]
        ph_img = ph_img_obj.get_data()
        mask = nib.load(maskFilename).get_data()

        px, py, pz = int(rad / voxel_size[0]), int(rad / voxel_size[1]), int(rad / voxel_size[2])
        x = np.linspace(-rad, rad, 2 * px + 1)
        y = np.linspace(-rad, rad, 2 * py + 1)
        z = np.linspace(-rad, rad, 2 * pz + 1)
        Y, X, Z = np.meshgrid(y, x, z)
        dist2 = (X ** 2 + Y ** 2 + Z ** 2)
        circ = dist2 <= rad ** 2

        mask_eroded = ndimage.binary_erosion(mask, circ)
        mask_dilated = ndimage.binary_dilation(mask, iterations=3)
        reliability = calculate_reliability(ph_img.astype('float32'), mask_dilated.astype('bool'))

        if 0:
            # absolute value of second difference dependent on image mean
            # dependent on background field
            # could try using std units to avoid this
            threshold_std = self.inputs.threshold_std
            mean = reliability[mask.astype('bool')].mean()
            std = reliability[mask.astype('bool')].std()
            threshold = mean + threshold_std * std
            threshold = threshold_std

        newmask = (((reliability < threshold) * mask + mask_eroded) > 0)
        newmask = ndimage.binary_fill_holes(newmask)
        newmask = ndimage.binary_opening(newmask).astype('int8')

        niftifile = nib.Nifti1Pair(newmask, ph_img_obj.affine)
        nib.save(niftifile, trimmed_mask_filename)
        niftifile = nib.Nifti1Pair(reliability, ph_img_obj.affine)
        nib.save(niftifile, reliability_filename)
        return runtime

    def _list_outputs(self):
        outputs = self.output_spec().get()
        outputs['trimmed_mask_filename'] = os.path.abspath(self.inputs.trimmed_mask_filename)
        outputs['reliability_filename'] = os.path.abspath(self.inputs.reliability_filename)
        return outputs
