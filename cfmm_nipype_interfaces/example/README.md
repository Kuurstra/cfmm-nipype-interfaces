**scaleimage_python.py** - shows a simple example of how to create an interface that executes python code.

**scaleimage_matlab.py** - shows a simple example of how to create an interface that executes 
matlab code from the command line.  The interface is designed to either run:
1) matlab_scripts/scaleimage_matlabscript.m using the matlab executable or
2) matlab_scripts/run_scaleimage_matlabscript.sh which uses mcr to run the matlab compiled code in matlab_scripts/run_scaleimage_matlabscript

Note that although you can name your matlab script anything, if you plan to use pip to import the cfmm_nipype_interfaces package in your nipype project, then you must include **matlabscript** in the name since this package's setup.py is designed to look for and include matlab files based on that keyword.


To see how to test and run the node, see **cfmm-nipyp-interfaces/test/example**.