"""
A simple example interface for executing python code in a pipeline.
"""

from nipype.interfaces.base import BaseInterface, CommandLineInputSpec, TraitedSpec, File
import traits.api as traits
import nibabel as nib
import os


class ScaleImageInputSpec(CommandLineInputSpec):
    # Inputs to the node. Here we can designate whether or not an error should be thrown if the input file does not
    # exist. We can also designate which inputs are mandatory and if there should be a default value.
    input_filename = File(exists=True, desc='Input image to scale.', copyFile=False, mandatory=True)
    scaling_factor = traits.Float(1.0, desc='Scaling factor.', mandatory=False, usedefault=True)
    output_filename = File(desc="Name of output file", mandatory=True)


class ScaleImageOutputSpec(TraitedSpec):
    # Tell the node the name of your output objects. You will define the file location of the outputs in _list_outputs()
    # The node will check to make sure your _run_interface() created the designated outputs in the correct location
    output_scaled = File(desc="Path to the saved output image.", exists=True)


class ScaleImage(BaseInterface):
    input_spec = ScaleImageInputSpec
    output_spec = ScaleImageOutputSpec

    def _run_interface(self, runtime):
        # get input images and values
        input_image_obj = nib.load(self.inputs.input_filename)
        input_image = input_image_obj.get_data()
        scaling = self.inputs.scaling_factor

        # process inputs
        output_image = input_image * scaling

        # save output files
        output_filename = self.inputs.output_filename
        nifti_obj = nib.Nifti1Pair(output_image, input_image_obj.affine)
        nib.save(nifti_obj, output_filename)
        return runtime

    def _list_outputs(self):
        # Define the file location of your output objects.
        # This helps the node enforce checks to make sure your _run_interface() actually does what you say it will do.
        # ie. The node will check you created the designated output files in the correct location.
        outputs = self.output_spec().get()
        outputs['output_scaled'] = os.path.abspath(self.inputs.output_filename)
        return outputs
