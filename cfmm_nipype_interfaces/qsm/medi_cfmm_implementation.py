"""
Western University CFMM implementation of MEDI from pyqsm package
"""

import traits.api as traits
import nibabel as nib
import numpy as np
import os
from nipype.interfaces.base import CommandLineInputSpec, BaseInterface, TraitedSpec, File
import os.path
import pyqsm

class MEDI_CFMM_InputSpec(CommandLineInputSpec):
    lfs_loc = File(exists=True, desc='Input lfs/rdf filename',
                   argstr="%s", position=1,
                   copyfile=False, mandatory=True)
    iMag_loc = File(exists=True, desc='Input avg magnitude for edge finding',
                    argstr="%s", position=2,
                    copyfile=False, mandatory=True)
    weight_loc = File(exists=True, desc='Weights for data fidelity in MEDI',
                      argstr="%s", position=6,
                      copyfile=False, mandatory=False)
    mask_loc = File(exists=True, desc='Mask (3D)', argstr="%s",
                    position=3, copyfile=False, mandatory=True)
    lamda = traits.Float(2.0, desc='Regularization parameter for MEDI', argstr='%s',
                         position=4, mandatory=False, usedefault=True)
    # note that MEDI wants it's rdf as lfs*te where te~0.004.
    # We use data that is not multiplied by te and so our data is 3 orders of magnitude larger
    # therefore if in matlab MEDI you use lambda of 500, then in this implementation try using 500*.004~2
    maxiter = traits.Int(200, desc='Max number of interations to perform in optimization', argstr='%s',
                         position=3, mandatory=False, usedefault=True)
    tol = traits.Float(0.0015,
                       desc='Tolerance. Quit optimization loop when change in susceptibility estimate is less than tolerance.',
                       argstr='%s',
                       position=3, mandatory=False, usedefault=True)
    CF = traits.Float(298060000.0, desc='Center frequency, used to return result in ppb', argstr='%s',
                      position=4, mandatory=False, usedefault=True)
    susceptibility_filename = File(desc="Output susceptibility filename", argstr='%s', position=5,
                                   mandatory=True)


class MEDI_CFMM_OutputSpec(TraitedSpec):
    susceptibility_filename = File(desc="Susceptibility estimate", exists=True)


class MEDI_CFMM(BaseInterface):
    input_spec = MEDI_CFMM_InputSpec
    output_spec = MEDI_CFMM_OutputSpec

    def _run_interface(self, runtime):
        rdf_file = self.inputs.lfs_loc
        mask_file = self.inputs.mask_loc
        iMag_file = self.inputs.iMag_loc
        WData_file = self.inputs.weight_loc
        alpha = self.inputs.lamda  # split bregman alpha is the weight on the data fidelity, MEDI uses lambda for this
        lamda = 2 * alpha  # split bregman uses lambda to refer to the weight which keeps bregman parameters close to their corresponding data
        maxiter = self.inputs.maxiter
        tol = self.inputs.tol
        CF = self.inputs.CF

        rdf_obj = nib.load(rdf_file)
        rdf = rdf_obj.get_data()
        voxel_size = rdf_obj.header['pixdim'][1:4]
        mask = nib.load(mask_file).get_data()
        iMag = nib.load(iMag_file).get_data().transpose(1, 0, 2)[:, ::-1, :]
        WData_sqrd = nib.load(WData_file).get_data()[..., 0] ** 2 * mask

        # create mask to protect edges from regularization
        percentage = 0.1  # keep 10% of voxels with highest gradient value
        WGradx, WGrady, WGradz = np.gradient(iMag, *voxel_size)
        wG = np.sqrt(WGradx ** 2 + WGrady ** 2 + WGradz ** 2)
        thresh = wG[mask.astype('bool')].max()
        num = ((mask * wG) > thresh).sum()
        den = (mask > 0).sum()
        while float(num) / den < percentage:
            thresh = thresh * 0.95
            num = ((mask * wG) > thresh).sum()
            den = (mask > 0).sum()
        wG = (mask * wG) < thresh
        WGradx = WGrady = WGradz = wG * mask

        # use CFMM local implementation of MEDI
        # when using matlab implementation of MEDI solver, weighting matrix causes numerical artifacts
        FOV = list(voxel_size * rdf.shape)
        Xmap = pyqsm.Liu2012.SplitBregmanL1Grad(rdf, FOV, WData_sqrd, WGradx, WGrady, WGradz, alpha, lamda,
                                                maxiter=maxiter, tol=tol)
        Xmap = Xmap / ((CF * 1e-9) * 2 * np.pi) * mask  # part per billion

        susceptibility_filename = self._list_outputs()['susceptibility_filename']
        niftifile = nib.Nifti1Pair(Xmap, rdf_obj.affine)
        nib.save(niftifile, susceptibility_filename)
        return runtime

    def _list_outputs(self):
        outputs = self.output_spec().get()
        outputs['susceptibility_filename'] = os.path.abspath(self.inputs.susceptibility_filename)
        return outputs

