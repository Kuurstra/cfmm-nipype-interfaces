"""
Cornell implementation of MEDI, implemented in matlab
"""

import traits.api as traits
import os
from nipype.interfaces.base import CommandLineInputSpec, BaseInterface, TraitedSpec, File
import os.path
from nipype.interfaces.base.traits_extension import isdefined
from ..matlab_runmode import MatlabRunMode
from ..MatlabBaseInterface import MatlabBaseInterface
from . import matlab_scripts


class MEDIInputSpec(CommandLineInputSpec):
    freq_loc = File(exists=True, desc='Input freq filename. Frequency stored in rad/s',
                    argstr="%s", copyfile=False, mandatory=True)
    mag_loc = File(exists=True, desc='Magnitude image for edge detection.',
                   argstr="%s", copyfile=False, mandatory=True)
    mask_loc = File(exists=True, desc='Mask (3D)', argstr="%s",
                    copyfile=False, mandatory=True)
    nstd_loc = File(exists=True, desc='N_std is used in weighting data fidelity term (see dataterm_mask.m)',
                    argstr="%s", copyfile=False, mandatory=False)
    gradient_mask_loc = File(exists=True, desc='Voxels to leave out of edge preserving regularizer',
                             argstr="%s", copyfile=False, mandatory=False)
    CF = traits.Float(298060000.0, desc='Center frequency in Hz, used to return result in ppb', argstr='%s',
                      mandatory=False, usedefault=True)
    alpha = traits.Float(0.3, desc='Regularization parameter for MEDI', argstr='%s',
                         mandatory=False, usedefault=True)
    merit = traits.Bool(True, desc='iteratively update optimization tuning parameters', argstr='%s',
                        mandatory=False, usedefault=True)
    B0_dir = traits.Int(3, desc='B0 direction (1, 2, or 3)', argstr='%s',
                        mandatory=False, usedefault=True)
    susceptibility_filename = File(desc="Output susceptibility filename", argstr='%s', mandatory=True)


class MEDIOutputSpec(TraitedSpec):
    susceptibility_filename = File(desc="Susceptibility estimate", exists=True)


class MEDI(MatlabBaseInterface):
    input_spec = MEDIInputSpec
    output_spec = MEDIOutputSpec

    def __init__(self, run_mode=MatlabRunMode.CONTAINER_MCR,
                 matlab_executable='/usr/local/matlab/R2016b/bin/matlab',
                 mcr_loc='/opt/mcr/v91', matlab_execution_override=None):
        matlab_scripts_dir = matlab_scripts.__path__._path[0]
        matlab_script_name = 'MEDI_matlabscript'
        super().__init__(matlab_scripts_dir, matlab_script_name, run_mode, matlab_executable, mcr_loc,
                         matlab_execution_override)

    def _run_interface(self, runtime):
        matlab_execution = self.command

        if isdefined(self.inputs.nstd_loc):
            nstd_loc = self.inputs.nstd_loc
        else:
            nstd_loc = 0

        if isdefined(self.inputs.gradient_mask_loc):
            gradient_mask_loc = self.inputs.gradient_mask_loc
        else:
            gradient_mask_loc = 0

        cmd = matlab_execution.replace('<params>', ' %s %s %s %s %s %s %s  %s %s %s' % (
            self.inputs.freq_loc,
            self.inputs.mag_loc,
            self.inputs.mask_loc,
            nstd_loc,
            gradient_mask_loc,
            self.inputs.CF,
            self.inputs.alpha,
            self.inputs.merit,
            self.inputs.B0_dir,
            self.inputs.susceptibility_filename
        ))
        os.system(cmd)
        return runtime

    def _list_outputs(self):
        outputs = self.output_spec().get()
        outputs['susceptibility_filename'] = os.path.abspath(self.inputs.susceptibility_filename)
        return outputs
