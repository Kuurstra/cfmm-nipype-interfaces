"""
runs Bilgic lab's matlab implementation of SS_TV
"""

import traits.api as traits
import os
from nipype.interfaces.base import CommandLineInputSpec, BaseInterface, TraitedSpec, File, CommandLine
import os.path
from ..matlab_runmode import MatlabRunMode
from ..MatlabBaseInterface import MatlabBaseInterface
from . import matlab_scripts


class SS_TVInputSpec(CommandLineInputSpec):
    freq_loc = File(exists=True, desc='Input freq filename. Frequency stored in rad/s',
                    argstr="%s", position=1,
                    copyfile=False, mandatory=True)
    mask_loc = File(exists=True, desc='Mask (3D)', argstr="%s",
                    position=2, copyfile=False, mandatory=True)
    reliability_mask_loc = File(exists=True, desc='Phase voxels to leave out of data fidelity term',
                                argstr="%s", position=3,
                                copyfile=False, mandatory=False)
    CF = traits.Float(298060000.0, desc='Center frequency in Hz, used to return result in ppb', argstr='%s',
                      position=4, mandatory=False, usedefault=True)
    alpha = traits.Float(0.3, desc='Regularization parameter for MEDI', argstr='%s',
                         position=5, mandatory=False, usedefault=True)
    mu1 = traits.Float(0.1, desc='Regularization parameter for MEDI', argstr='%s',
                       position=6, mandatory=False, usedefault=True)
    mu2 = traits.Float(0.1, desc='Regularization parameter for MEDI', argstr='%s',
                       position=7, mandatory=False, usedefault=True)
    B0_dir = traits.Int(3, desc='B0 direction (1, 2, or 3)', argstr='%s',
                        position=8, mandatory=False, usedefault=True)
    smv_min_rad = traits.Float(1.0, desc='Regularization parameter for MEDI', argstr='%s',
                               position=9, mandatory=False, usedefault=True)
    smv_max_rad = traits.Float(5.0, desc='Regularization parameter for MEDI', argstr='%s',
                               position=10, mandatory=False, usedefault=True)
    smv_step_rad = traits.Float(1.0, desc='Regularization parameter for MEDI', argstr='%s',
                                position=11, mandatory=False, usedefault=True)
    max_outer_iter = traits.Int(200, desc='B0 direction (1, 2, or 3)', argstr='%s',
                                position=12, mandatory=False, usedefault=True)
    tol_soln = traits.Float(0.1, desc='Regularization parameter for MEDI', argstr='%s',
                            position=13, mandatory=False, usedefault=True)
    susceptibility_filename = File(desc="Output susceptibility filename", argstr='%s', position=14, mandatory=True)


class SS_TVOutputSpec(TraitedSpec):
    susceptibility_filename = File(desc="Susceptibility estimate", exists=True)


class SS_TV(MatlabBaseInterface):
    input_spec = SS_TVInputSpec
    output_spec = SS_TVOutputSpec

    def __init__(self, run_mode=MatlabRunMode.CONTAINER_MCR,
                 matlab_executable='/usr/local/matlab/R2016b/bin/matlab',
                 mcr_loc='/opt/mcr/v91', matlab_execution_override=None):
        matlab_scripts_dir = matlab_scripts.__path__._path[0]
        matlab_script_name = 'SS_TV_matlabscript'
        super().__init__(matlab_scripts_dir, matlab_script_name, run_mode, matlab_executable, mcr_loc,
                         matlab_execution_override)

    def _run_interface(self, runtime):
        matlab_execution = self.command

        cmd = matlab_execution.replace('<params>', ' %s %s %s %s %s %s %s %s %s %s %s %s %s %s' % (
            self.inputs.freq_loc,
            self.inputs.mask_loc,
            self.inputs.reliability_mask_loc,
            self.inputs.CF,
            self.inputs.alpha,
            self.inputs.mu1,
            self.inputs.mu2,
            self.inputs.B0_dir,
            self.inputs.smv_min_rad,
            self.inputs.smv_max_rad,
            self.inputs.smv_step_rad,
            self.inputs.max_outer_iter,
            self.inputs.tol_soln,
            self.inputs.susceptibility_filename
        ))
        os.system(cmd)
        return runtime

    def _list_outputs(self):
        outputs = self.output_spec().get()
        outputs['susceptibility_filename'] = os.path.abspath(self.inputs.susceptibility_filename)
        return outputs
