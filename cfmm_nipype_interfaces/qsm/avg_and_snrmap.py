"""
open list of mag images and return the average and weights based on local neighbourhood snr
"""

from scipy import ndimage
import traits.api as traits
import nibabel as nib
import numpy as np
import os, sys
from nipype.interfaces.base import CommandLineInputSpec, BaseInterface, TraitedSpec, InputMultiPath, File
import os.path


class GetAvgAndSNRMapInputSpec(CommandLineInputSpec):
    mag = InputMultiPath(File(exists=True), desc='A list of magnitude echoes',
                         copyfile=False, mandatory=True)
    snr_window_sz = traits.Float(15, desc='The size of the window used to calculate local SNR in mm',
                                 mandatory=False, usedefault=True)
    avg_out_filename = File(desc="Output file", mandatory=True)
    snr_map_out_filename = File(desc="Output file", mandatory=True)


class GetAvgAndSNRMapOutputSpec(TraitedSpec):
    avg_out_filename = File(desc="Output file containing the estimated Frequency",
                            exists=True)
    snr_map_out_filename = File(desc="Output file containing the estimated Frequency",
                                exists=True)


class GetAvgAndSNRMap(BaseInterface):
    input_spec = GetAvgAndSNRMapInputSpec
    output_spec = GetAvgAndSNRMapOutputSpec

    def _run_interface(self, runtime):
        mag_filename_list = self.inputs.mag
        snr_window_sz_mm = self.inputs.snr_window_sz
        avg_out_filename = self._list_outputs()['avg_out_filename']
        snr_map_out_filename = self._list_outputs()['snr_map_out_filename']
        ne = len(mag_filename_list)
        mag_img_obj = nib.load(mag_filename_list[0])
        shape = mag_img_obj.shape
        voxel_size = mag_img_obj.header['pixdim'][1:4]
        snr_window_sz_vxl = np.ceil(snr_window_sz_mm / np.array(voxel_size)).astype('int')
        avg = np.zeros(shape)
        weight_avg = 0
        weight = np.empty(shape + (ne,))
        count = 0
        mag_filename_list.sort()
        eps = sys.float_info.min ** (1.0 / 4)
        for imgloc in mag_filename_list:
            curr_img = (nib.load(imgloc).get_data()).astype('float')
            curr_img_avg = np.abs(curr_img).mean()
            local_mean = ndimage.uniform_filter(curr_img, snr_window_sz_vxl)
            local_sqr_mean = ndimage.uniform_filter(curr_img ** 2, snr_window_sz_vxl)
            local_var = local_sqr_mean - local_mean ** 2 + eps
            weight[..., count] = local_mean / local_var + eps  # snr
            avg += curr_img_avg * curr_img  # weighted avg, later echos weighted less
            weight_avg += curr_img_avg
            count += 1
        avg = avg / weight_avg

        niftifile = nib.Nifti1Pair(avg, mag_img_obj.affine)
        nib.save(niftifile, avg_out_filename)
        niftifile = nib.Nifti1Pair(weight, mag_img_obj.affine)
        nib.save(niftifile, snr_map_out_filename)

        return runtime

    def _list_outputs(self):
        outputs = self.output_spec().get()
        outputs['avg_out_filename'] = os.path.abspath(self.inputs.avg_out_filename)
        outputs['snr_map_out_filename'] = os.path.abspath(self.inputs.snr_map_out_filename)
        return outputs
