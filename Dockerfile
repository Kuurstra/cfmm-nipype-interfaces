FROM ubuntu:xenial

COPY ./*.py /code/
COPY requirements.txt /code/

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y --no-install-recommends \
sudo \
wget \
apt-utils \
curl \
git \
dos2unix \
tree \
zip \
unzip \
make \
cmake \
bzip2 \
build-essential \
libtool \
autotools-dev \
automake \
autoconf \
tzdata \
git \
unzip \
ca-certificates \
python3 \
python3-dev \
python3-pip

#pip dropped support for Python 3.5
RUN curl -fsSL https://bootstrap.pypa.io/pip/3.5/get-pip.py | python3

RUN pip install virtualenv

#=============================================
#install mcr for matlab qsm code
#=============================================
#install mcr for matlab dipole inversion code
ENV MATLAB_VERSION R2016b
RUN mkdir /opt/mcr_install && \
    mkdir /opt/mcr && \
    wget -P /opt/mcr_install https://ssd.mathworks.com/supportfiles/downloads/${MATLAB_VERSION}/deployment_files/${MATLAB_VERSION}/installers/glnxa64/MCR_${MATLAB_VERSION}_glnxa64_installer.zip && \
    unzip -q /opt/mcr_install/MCR_${MATLAB_VERSION}_glnxa64_installer.zip -d /opt/mcr_install && \
    /opt/mcr_install/install -destinationFolder /opt/mcr -agreeToLicense yes -mode silent && \
    rm -rf /opt/mcr_install /tmp/*
#=============================================

#mcr
#ENV MCR_VERSION v91
#ENV LD_LIBRARY_PATH /opt/mcr/${MCR_VERSION}/runtime/glnxa64:/opt/mcr/${MCR_VERSION}/bin/glnxa64:/opt/mcr/${MCR_VERSION}/sys/os/glnxa64:/opt/mcr/${MCR_VERSION}/sys/opengl/lib/glnxa64
ENV MCR_INHIBIT_CTF_LOCK 1


