stages:
  - build
  - test
  - release

variables:
  # $CI_REGISTRY_IMAGE is resolved to the address of the registry tied to this project.
  # Since $CI_COMMIT_REF_NAME resolves to the branch or tag name, and your branch-name can contain forward slashes
  # (e.g., feature/my-feature), it is safer to use $CI_COMMIT_REF_SLUG as the image tag.
  # Since image tags cannot contain forward slashes.
  CONTAINER_TEST_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  CONTAINER_RELEASE_IMAGE: $CI_REGISTRY_IMAGE:latest

build-docker-image:
  stage: build
  image: docker:19.03.1
  services:
    - docker:19.03.1-dind
  variables:
    # Use TLS https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#tls-enabled

    # When using dind service we need to instruct docker, to talk with the
    # daemon started inside of the service. The daemon is available with
    # a network connection instead of the default /var/run/docker.sock socket.
    # The 'docker' hostname is the alias of the service container as described at
    # https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#accessing-the-services
    # For non-Kubernetes executors, we use tcp://docker:2375
    DOCKER_HOST: tcp://docker:2376
    # Specify to Docker where to create the certificates, Docker will
    # create them automatically on boot, and will create
    # `/certs/client` that will be shared between the service and
    # build container.
    DOCKER_TLS_CERTDIR: "/certs"
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    # gitlab doesn't seem to have a great way of caching docker layers from inside the dind service
    # https://gitlab.com/gitlab-org/gitlab-foss/issues/17861
    # https://gitlab.com/gitlab-org/gitlab-runner/issues/1107
    # just pull the latest image and use docker's caching tools
    - docker pull $CONTAINER_RELEASE_IMAGE || true
    - docker build --cache-from $CONTAINER_RELEASE_IMAGE -t $CONTAINER_TEST_IMAGE -f Dockerfile .
    - docker push $CONTAINER_TEST_IMAGE
  only:
    - pushes

test-python3:
  stage: test
  image: $CONTAINER_TEST_IMAGE
  variables:
    PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  cache:
    key: "$CI_JOB_NAME"
    paths:
      - .cache/pip
      - venv
  before_script:
    - virtualenv venv
    - source venv/bin/activate
  script:
  - pip install numpy==1.16.2
  - pip install -r requirements.txt
  - python -m unittest discover test
  only:
    - pushes

release-docker-image:
  stage: release
  image: docker:19.03.1
  services:
    - docker:19.03.1-dind
  variables:
    DOCKER_HOST: tcp://docker:2376
    DOCKER_TLS_CERTDIR: "/certs"
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker pull $CONTAINER_TEST_IMAGE
    - docker tag $CONTAINER_TEST_IMAGE $CONTAINER_RELEASE_IMAGE
    - docker push $CONTAINER_RELEASE_IMAGE
  only:
    - master
