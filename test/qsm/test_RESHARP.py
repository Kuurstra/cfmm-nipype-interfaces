import nipype.pipeline.engine as pe
import os
import unittest
from cfmm_nipype_interfaces.qsm import RESHARP
from cfmm_nipype_interfaces import MatlabRunMode
from cfmm_nipype_interfaces.numerical_phantoms.qsmphantom import create_phantom, phantom_type
import numpy as np
import nibabel as nib
import tempfile


class TestRESHARP(unittest.TestCase):
    def __init__(self, *args, run_mode=MatlabRunMode.CONTAINER_MCR, **kwargs):
        self.run_mode = run_mode
        super(TestRESHARP, self).__init__(*args, **kwargs)

    def setUp(self):
        tmpdir_obj = tempfile.TemporaryDirectory()
        self.tmpdir_obj = tmpdir_obj
        self.tmpdir = tmpdir_obj.name
        dim_sz = 100
        FOV_mm = 320
        fieldmap_img, ellipsoids, _, lfs_img = create_phantom(dim_sz, FOV_mm, ptype=phantom_type.FREQ)
        main_ellipsoid = ellipsoids[..., 0]
        self.fieldmap = os.path.join(self.tmpdir, 'freq.nii.gz')
        affine = np.eye(4) * np.diag([FOV_mm / dim_sz, FOV_mm / dim_sz, FOV_mm / dim_sz, 1])
        img = nib.Nifti1Image(fieldmap_img, affine)
        img.header.set_xyzt_units('mm')
        nib.save(img, self.fieldmap)
        self.mask = os.path.join(self.tmpdir, 'mask.nii.gz')
        img = nib.Nifti1Image(main_ellipsoid.astype('int8'), affine)
        img.header.set_xyzt_units('mm')
        nib.save(img, self.mask)
        self.lfs = lfs_img

    def test_calculation(self):
        matlab_executable = '/usr/local/matlab/R2016b/bin/matlab'
        local_mcr_location = '/storage/akuurstr/MATLAB_R2016b/v91/'

        resharp_node = pe.Node(interface=RESHARP(run_mode=self.run_mode, matlab_executable=matlab_executable,
                                                 mcr_loc=local_mcr_location), name="test_RESHARP")
        resharp_node.inputs.freq_loc = self.fieldmap
        resharp_node.inputs.mask_loc = self.mask
        resharp_node.inputs.radius = 5.0
        resharp_node.inputs.alpha = 0.0001
        resharp_node.inputs.LFS_filename = os.path.join(self.tmpdir, 'lfs.nii.gz')

        resharp_node_result = resharp_node.run()
        img_loc = resharp_node_result.outputs.LFS_filename
        measured = nib.load(img_loc).get_data()

        mask_loc = resharp_node_result.outputs.LFS_mask_filename
        mask = nib.load(mask_loc).get_data().astype('bool')

        abs_error = np.abs(measured-self.lfs)
        # the algorithm doesn't perform well when looking at percent_error, so going to not use that as a test
        # percent_error = abs_error/(np.abs(self.lfs)+np.finfo('float').eps)*100
        # percent_error = percent_error[mask]

        # this is an easy test, keep an eye out for a better unit test
        abs_error = abs_error[mask]
        normalize_term = np.abs(self.lfs[mask])
        self.assertLess(abs_error.mean()/normalize_term.mean(),0.9)

if __name__ == '__main__':
    # make sure you run this file and not pycharm's "unittests for ..."  or "unittests in ..."
    suite = unittest.TestSuite()
    # suite.addTest(TestRESHARP('test_calculation', run_mode=MatlabRunMode.LOCAL_MATLAB))
    suite.addTest(TestRESHARP('test_calculation', run_mode=MatlabRunMode.LOCAL_MCR))
    runner = unittest.TextTestRunner()
    runner.run(suite)
