import nipype.pipeline.engine as pe
import unittest
from cfmm_nipype_interfaces.text_files import replace_slash

class Testreplace_slash(unittest.TestCase):
    def test(self):
        test_example = 'this/is/a/test'
        test = pe.Node(interface=replace_slash, name="test_replace_slash")
        test.inputs.filename = test_example
        result = test.run()
        self.assertEqual(result.outputs.renamed,'this_is_a_test')

if __name__=='__main__':
    unittest.main()