import nipype.pipeline.engine as pe
import unittest
from cfmm_nipype_interfaces.example.scaleimage_python import ScaleImage
import nibabel as nib
import numpy as np
import tempfile
import os
import numpy.testing as npt


class TestScaleImage(unittest.TestCase):
    def setUp(self):
        # anything that all tests will need
        self.input_img = np.ones((3,3,3))
        affine = np.eye(4)
        input_img_nifti_obj = nib.Nifti1Pair(self.input_img, affine)
        self.tmpdir_obj = tempfile.TemporaryDirectory()
        self.tmpdir = self.tmpdir_obj.name
        self.test_input_image = os.path.join(self.tmpdir, 'test_input.nii.gz')
        nib.save(input_img_nifti_obj, self.test_input_image)

    def test_scale_interface(self):
        test = pe.Node(interface=ScaleImage(), name="test_ScaleImage")
        test.inputs.input_filename = self.test_input_image
        test.inputs.scaling_factor = 2.0
        test.inputs.output_filename = os.path.join(self.tmpdir,"my_scaled_image.nii.gz")

        test.run()

        node_result = nib.load(test.inputs.output_filename).get_data()
        expected_result = self.input_img * test.inputs.scaling_factor
        npt.assert_almost_equal(expected_result, node_result)

    def tearDown(self):
        # delete test files?
        # because we used a temproary directory, there's no need to manually delete files created during testing
        pass

if __name__ == '__main__':
    unittest.main()
